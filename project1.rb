default =  "Welcome to the library \nOptions \n \t1. List Books \n\t2. Edit a Book \n\t3. Create a book \n\t4. Delete a book \n\t5. Exit \n"
puts "#{default}"

list = ['title of book 1', 'title of book 2', 'title of book 3']

#Choosing an option
puts"Select an option from 1-5"

should_exit = false
while should_exit == false
  print "Option: "
  user_input = gets.chomp.to_i
  if user_input == 1
  list.each_with_index {|val, index| puts "#{index+1}. #{val}"}
  end

  if user_input == 3
    puts "New Book"
        print "\ttitle: "
        new = $stdin.gets.chomp
        print "\tAuthor: "
        auth = $stdin.gets.chomp
        puts "title: #{new}"
        list.push(new)
  end

  if user_input == 2
    puts "Which book would you like to edit?"
    c = gets.chomp.to_i
    d = c -1

    puts "Current book with book id #{c}"
    puts "\ttitle: #{list[d]}"

    puts "Insert the title and author Name"
    print "\t Enter Title : "

    b = gets.chomp
    print "\t Enter Author : "
    l = gets.chomp

    list.delete_at(d)
    list.insert(d, b)
  end

  if user_input == 4
    puts "Delete a book by ID"
    print "\t Book ID to be deleted: "

    c = gets.chomp.to_i
    b = c-1
    #a.delete_at(c)
    #a.insert(c, b)
    #a.fetch(3)
    list.delete_at(b)
    #puts "#{list}"
  end

  if user_input == 5
    should_exit = true
  end
  puts '------------------'
  puts "#{default}"
end
